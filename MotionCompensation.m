clear;
foldername = 'video_sequence';
path(path, foldername);
imageseq = dir ([foldername '/' 'foreman*.bmp']);                           % put all foreman image in imageseq struct
for seq = 1:length(imageseq)
    imageseq(seq).im = double(imread(imageseq(seq).name));                  % store original RGB image in the struct field 'im'
end
count = 1;
for coe = [0.2,0.5,0.7,1,1.2,1.5,1.7,2]; % change
% pre-allocation
bit_rate = zeros(1,seq);
PSNR = zeros(1,seq);
% do following to all frames
for proc = 1:seq-1 % 1:20
    % RGB2YCbCr to next frame     
    [ ict_curr(:,:,1), ict_curr(:,:,2), ict_curr(:,:,3) ] =...
        ictRGB2YCbCr( imageseq(proc+1).im(:,:,1), imageseq(proc+1).im(:,:,2), imageseq(proc+1).im(:,:,3) );
    if proc == 1
        % transform coding directly to first frame
        % RGB2YCbCr to first frame 
        [ ict_prev(:,:,1), ict_prev(:,:,2), ict_prev(:,:,3) ] = ...
            ictRGB2YCbCr( imageseq(proc).im(:,:,1), imageseq(proc).im(:,:,2), imageseq(proc).im(:,:,3) );
        % transform coding and huffman encode to first frame 
        [ BinaryTree_first, enc_first, len_first ] = IntraEncode( ict_prev, coe );
        % calculate bit rate, no encoded mv for first frame
        bit_rate(proc) = 8 * length(enc_first) / ( size(ict_curr,1) * size(ict_curr,2) );
        % huffman decode and inverse transform coding to first frame(ict_out)
        recon_ict = IntraDecode( enc_first, BinaryTree_first, len_first, size(ict_curr), coe );
        % recon_ict -> recon_RGB
        [ recon(:,:,1), recon(:,:,2), recon(:,:,3) ] = ...
            ictYCrCb2RGB( recon_ict(:,:,1), recon_ict(:,:,2), recon_ict(:,:,3) );
        % calculate PSNR for first frames
        PSNR(proc) = calcPSNR( imageseq(proc).im, recon, 8 );
        bitrate_still(count) = bit_rate(proc)
        PSNR_still(count) = PSNR(proc)
        
        % use 1st frame to predict 2nd frame -> motion vector, error, reconstruted 2nd frame
        [ mv, error ] = blockmatch( recon_ict, ict_curr );   
    else
        % use reconstruted previous frame to predict current frame -> motion vector, error, reconstruted current frame
        [ mv, error ] = blockmatch( recon_ict, ict_curr );     
    end
    
    % deal with motion vector
    % use current mv vector to train its huffman table then encode

    PMF_mv = stats_marg( mv, -4:4 );
    [BinaryTree_mv, ~, BinCode_mv, Codelengths_mv] = buildHuffman(PMF_mv);
    enc_mv = enc_huffman_new( mv(:) + 5, BinCode_mv, Codelengths_mv);
    % decode Huffman at receiver 
    dec_mv = reshape( dec_huffman_new( enc_mv, BinaryTree_mv, length(mv(:)) ), [], 2 )  - 5;
    
    % deal with error
    % transform coding and huffman coding to current error frame
    [ BinaryTree_error, enc_error, len_error ] = IntraEncode( error, coe ); 
    % calculate bit rate, sum of encoded mv and encoded error
    bit_rate(proc+1) = 8 * ( length(enc_mv) + length(enc_error) ) / ( size(ict_curr,1) * size(ict_curr,2) );
    % huffman decode and inverse transform coding to error frame(ict_out)
    recon_error = IntraDecode( enc_error, BinaryTree_error, len_error, size(ict_curr), coe );
    
    % predict current frame from reconstructed previous frame and decoded motion vector
    pred_frame = PredBasedMV( recon_ict, dec_mv );
    % reconstruct current frame by adding pred + error
    recon_ict = pred_frame + recon_error;
    % recon_ict -> recon_RGB
    [ recon(:,:,1), recon(:,:,2), recon(:,:,3) ] =...
        ictYCrCb2RGB( recon_ict(:,:,1), recon_ict(:,:,2), recon_ict(:,:,3) );
    % calculate PSNR
    PSNR(proc+1) = calcPSNR( imageseq(proc+1).im, recon, 8 );
    
end
% average bit rate
bit_rate_avg = mean(bit_rate);
bitrate_video(count) = bit_rate_avg
% average PSNR
PSNR_avg = mean(PSNR);
PSNR_video(count) = PSNR_avg
count = count + 1;
end
figure; hold on;
plot(bitrate_still, PSNR_still,'b+-');
plot(bitrate_video, PSNR_video,'r*-');
save('5.mat','bitrate_still','PSNR_still','bitrate_video','PSNR_video');