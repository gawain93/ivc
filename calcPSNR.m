 function PSNR = calcPSNR( ORIGINAL_image, RECONSTRUCTED_image, B )
[height, width, dim] = size(ORIGINAL_image);
image_deviation = ORIGINAL_image - RECONSTRUCTED_image;
image_deviation_square = image_deviation.^2;
MSE = sum(image_deviation_square(:))/(height * width * dim);
PSNR = 20 * log10(2^B -1) - 10 * log10(MSE);
end