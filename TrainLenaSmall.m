clear
%% train lena_small
coe = 1;
foldername = 'video_sequence';
path(path, foldername);
imt = double( imread( 'foreman0020.bmp' ) );
[ ict(:,:,1), ict(:,:,2), ict(:,:,3) ] = ictRGB2YCbCr( imt(:,:,1), imt(:,:,2), imt(:,:,3) );
block_out = block8x8( ict );

dct_out = zeros( size( block_out ) );
quant_out = zeros( size( block_out ) );
zigzag_out = zeros( 1, 64, size( block_out, 3 ) );
runlevel_out = [];
for i = 1:size( block_out, 3 )
    dct_out(:,:,i) = dct8x8( block_out(:,:,i) );
    if i <= size( block_out, 3 )/3
        quant_out(:,:,i) = QuantL8x8( dct_out(:,:,i), coe );
    else
        quant_out(:,:,i) = QuantC8x8( dct_out(:,:,i), coe );
    end
    zigzag_out(:,:,i) = ZigZag8x8( quant_out(:,:,i) );
    runlevel_out = [ runlevel_out ZeroRunEnc( zigzag_out(:,:,i) )];
end
PMF = stats_marg( runlevel_out, -3000:1000 );
[BinaryTree, HuffCode, BinCode, Codelengths] = buildHuffman(PMF);