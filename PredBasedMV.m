function pred_frame = PredBasedMV( prev_frame, mv )
% prev_frame - 3D image, mv - all mvs in one image
% pre-allocation
sz = size(prev_frame);
pred_frame = zeros(sz);
% pad previous frame
prev_frame = padarray(prev_frame, [4,4], 'replicate');
% number of block
num = 1;
for r = 1:8:sz(1)
    for c = 1:8:sz(2)
        index = [r,c] + mv(num,:) + [4,4]; % predicted index in previous frame
        pred_frame( r:r+7, c:c+7, : ) = prev_frame( index(1):index(1)+7, index(2):index(2)+7, : ); % extract predicted block correpondingly from previous frame 
        num = num + 1; % deal with next block
    end
end
end