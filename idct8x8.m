function dct_out = idct8x8( dct_in )
% in, out matrix 8 * 8 block
dct_col = idct(dct_in); % column
dct_row = idct(dct_col'); % row
dct_out = dct_row';

end