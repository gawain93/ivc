coe_set = [1/3 1/2.5 1/2 1/1.5 1 1.5 2 2.5 3];
trial = 1;
bit_rate_vec = zeros(size(coe_set));
PSNR_vec = zeros(size(coe_set));
while trial <= length(coe_set)
    coe = coe_set(trial)
    TrainLenaSmall;
    TestLena;
    bit_rate_vec(trial) = bit_rate;
    PSNR_vec(trial) = PSNR;
    trial = trial + 1;
end
plot(bit_rate_vec, PSNR_vec, '*-');
xlabel('bpp'); ylabel('PSNR[dB]');