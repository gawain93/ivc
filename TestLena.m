%% encode lena
% load Bincode; load Codelengths;
im = double( imread( 'foreman0020.bmp' ) );
[ ict_im(:,:,1), ict_im(:,:,2), ict_im(:,:,3) ] = ictRGB2YCbCr( im(:,:,1), im(:,:,2), im(:,:,3) );
block_out_im = block8x8( ict_im );
dct_out_im = zeros( size( block_out_im ) );
quant_out_im = zeros( size( block_out_im ) );
zigzag_out_im = zeros( 1, 64, size( block_out_im, 3 ) );
runlevel_out_im = [];
%coe=99; %%
for i = 1:size( block_out_im, 3 )
    dct_out_im(:,:,i) = dct8x8( block_out_im(:,:,i) );
    if i <= size( block_out_im, 3 )/3
        quant_out_im(:,:,i) = QuantL8x8( dct_out_im(:,:,i), coe );
    else
        quant_out_im(:,:,i) = QuantC8x8( dct_out_im(:,:,i), coe );
    end
    zigzag_out_im(:,:,i) = ZigZag8x8( quant_out_im(:,:,i) );
    runlevel_out_im = [ runlevel_out_im ZeroRunEnc( zigzag_out_im(:,:,i) )];
end
enc = enc_huffman_new( runlevel_out_im + 3001, BinCode, Codelengths);
bit_rate = 8 * length(enc) / ( size(im,1) * size(im,2) )

%% decode lena
dec = dec_huffman_new( enc, BinaryTree, length(runlevel_out_im) ) - 3001;
derunlevel_out = ZeroRunDec( dec, 64 );
derunlevel_out = reshape( derunlevel_out, 1, 64, [] );
dezigzag_out = zeros( 8, 8, size(derunlevel_out,3) );
dequant_out = zeros( 8, 8, size(derunlevel_out,3) );
idct_out = zeros( 8, 8, size(derunlevel_out,3) );
ictr = zeros( size(ict_im) );
for j = 1:size( derunlevel_out, 3 )
    dezigzag_out(:,:,j) = DeZigZag8x8( derunlevel_out(:,:,j) );
    if j <= size( derunlevel_out, 3 )/3
        dequant_out(:,:,j) = DeQuantL8x8( dezigzag_out(:,:,j), coe );
    else
        dequant_out(:,:,j) = DeQuantC8x8( dezigzag_out(:,:,j), coe );
    end
    idct_out(:,:,j) = idct8x8( dequant_out(:,:,j) );
    dtmp = size(ict_im,1)*size(ict_im,2)/64;
    rtmp = size(ict_im,2)/8;
    dr = ceil(j/dtmp); rr = ceil((j-(dr-1)*dtmp)/rtmp); cr = j-(dr-1)*dtmp-(rr-1)*rtmp;
    ictr(8*(rr-1)+1:8*rr,8*(cr-1)+1:8*cr,dr) = idct_out(:,:,j);
end
[ imr(:,:,1), imr(:,:,2), imr(:,:,3) ] = ictYCrCb2RGB( ictr(:,:,1), ictr(:,:,2), ictr(:,:,3) );
% imshow(imr/255);
PSNR = calcPSNR( im, imr, 8 )