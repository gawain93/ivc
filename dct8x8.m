function dct_out = dct8x8( dct_in )
% in, out matrix 8 * 8 block
dct_col = dct(dct_in); % column
dct_row = dct(dct_col'); % row
dct_out = dct_row';

end