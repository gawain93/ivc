function ictr = IntraDecode( enc, BinaryTree, len, sz, coe )
% huffman decode and inverse transform coding
% decode runlevel_out from huffman stream
dec = dec_huffman_new( enc, BinaryTree, len ) - 3001;
% decode zigzag_out from runlevel_out, i.e. inverse run level 
derunlevel_out = ZeroRunDec( dec, 64 );
derunlevel_out = reshape( derunlevel_out, 1, 64, [] );
% pre-allocation
dezigzag_out = zeros( 8, 8, size(derunlevel_out,3) );
dequant_out = zeros( 8, 8, size(derunlevel_out,3) );
idct_out = zeros( 8, 8, size(derunlevel_out,3) );
ictr = zeros( sz );
% do following to all blocks
for j = 1:size( derunlevel_out, 3 )
    % inverse zigzag
    dezigzag_out(:,:,j) = DeZigZag8x8( derunlevel_out(:,:,j) );
    % dequantization
    if j <= size( derunlevel_out, 3 )/3                  % luminance
        dequant_out(:,:,j) = DeQuantL8x8( dezigzag_out(:,:,j), coe );
    else                                                 % chrominance
        dequant_out(:,:,j) = DeQuantC8x8( dezigzag_out(:,:,j), coe );
    end
    % inverse dct
    idct_out(:,:,j) = idct8x8( dequant_out(:,:,j) );
    % order every block back to original image size
    dtmp = sz(1)*sz(2)/64;
    rtmp = sz(2)/8;
    dr = ceil(j/dtmp); rr = ceil((j-(dr-1)*dtmp)/rtmp); cr = j-(dr-1)*dtmp-(rr-1)*rtmp;
    ictr(8*(rr-1)+1:8*rr,8*(cr-1)+1:8*cr,dr) = idct_out(:,:,j);
end

end

