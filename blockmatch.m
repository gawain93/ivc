function [ mv, error ] = blockmatch( ict_prev, ict_curr )
% clear
% path(path, 'video_sequence');
% curr = double( imread( 'foreman0021.bmp'  ));
% [ ict_curr(:,:,1), ict_curr(:,:,2), ict_curr(:,:,3) ] = ictRGB2YCbCr( curr(:,:,1), curr(:,:,2), curr(:,:,3) );
% ict_prev = ictr;

% pad previous frame
ict_prev = padarray(ict_prev, [4,4], 'replicate');
% use luminance dimension to find motion vectors
lum_prev = ict_prev(:,:,1);
lum_curr = ict_curr(:,:,1);
% total number of blocks = mvs
num_block = size(lum_curr,1) * size(lum_curr,2)/(8*8);
% pre-allocation
ssd = zeros(9,9);
mv = zeros(num_block,2);
pred = zeros(size(ict_curr));
% do following to all blocks
num = 1;
for r = 1:8:size(lum_curr,1)
    for c = 1:8:size(lum_curr,2)
        block = lum_curr(r:r+7,c:c+7);   % start index = (r,c) in current frame
        for sr = 0:8
            for sc = 0:8  % 81 possible blocks in previous frame
                sd = (lum_prev(r+sr:r+sr+7,c+sc:c+sc+7) - block).^2;
                ssd(sr+1,sc+1) = sum(sd(:)); % 81 possible ssd values
            end
        end
       [ ~, I ] = min(ssd(:)); % find the minimum ssd
       mv_block = [ I-(ceil(I/9)-1)*9 ceil(I/9) ] - [5 5]; % convert index I into [row, col], minus offset to have range [-4,4]
       mv( num,: ) = mv_block; % put mv for all blocks together
       pred_index = [r c] + mv_block + [4,4]; % corresponding index in previous frame, +[4,4] due to pading
       pred( r:r+7, c:c+7, : ) = ict_prev( pred_index(1):pred_index(1)+7, pred_index(2):pred_index(2)+7,: ); % extract predicted block correpondingly from previous frame
       num = num + 1;
    end
end
error = ict_curr - pred;
end